package uk.ac.uea.informationaggregator;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;


public class MainActivity extends AppCompatActivity {




    public enum TabType
    {
        SHOP, TRAVEL, FOOD, PERSONAL
    }

    private static TabsList tabsList = new TabsList();
    private ProgressDialog mProgressDialog;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);


        //If finished here I would read in the sources file which contains the urls that woul
        //populate the personal tab and add them all to tabsList[4]


        new getAllContent().execute();



    }


    private class getAllContent extends AsyncTask<Void, Void, Void> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            mProgressDialog = new ProgressDialog(MainActivity.this);
            mProgressDialog.setMessage("Updating...");
            mProgressDialog.setIndeterminate(false);
            mProgressDialog.show();
        }

        @Override
        protected Void doInBackground(Void... params) {
            tabsList.getTab(0).updateTab();
            tabsList.getTab(1).updateTab();
            tabsList.getTab(2).updateTab();
            tabsList.getTab(3).updateTab();
            return null;
        }


        @Override
        protected void onPostExecute(Void result) {
            mProgressDialog.dismiss();
        }
    }





    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_addRemove) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    public void gotoTravel(View view)
    {
        Intent intent = new Intent(this,TabActivity.class);
        intent.putExtra("thisTabIs", TabType.TRAVEL);
        startActivity(intent);
    }

    public void gotoShop(View view)
    {
        Intent intent = new Intent(this,TabActivity.class);
        intent.putExtra("thisTabIs", TabType.SHOP);
        startActivity(intent);

    }

    public void gotoFood(View view)
    {
        Intent intent = new Intent(this,TabActivity.class);
        intent.putExtra("thisTabIs", TabType.FOOD);
        startActivity(intent);
    }

    public void gotoPersonal(View view)
    {
        Intent intent = new Intent(this,TabActivity.class);
        intent.putExtra("thisTabIs", TabType.PERSONAL);
        startActivity(intent);
    }


    public static Tab getTab(int tab)
    {
        return tabsList.getTab(tab);
    }

    public void addRemoveSource(MenuItem item){
        Intent intent = new Intent(this,AddRemoveActivity.class);
        startActivity(intent);
    }

}
