package uk.ac.uea.informationaggregator;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.select.Elements;

/**
 * Created by sky13nmu on 27/11/2015.
 */
public class Information {

    private String url;
    private Document document;

    Information(String givenUrl)
    {
        url = givenUrl;
    }

    public String getURL()
    {
        return url;
    }

    public Document getDocument()
    {
        return document;
    }

    public void setURL(String newURL)
    {
        url = newURL;
    }

    public void updateInfo()
    {
        try{
            document = Jsoup.connect(url).timeout(60000).get();
//            document = Jsoup.connect(url).get();
//            other = document.title();
//            System.out.println(document);
            System.out.println();
        }catch(Exception e) {
            System.out.println("THIS ERROR IS" + e);
        }
    }



}
