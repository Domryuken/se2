package uk.ac.uea.informationaggregator;

import android.content.Intent;
import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.app.ProgressDialog;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;

public class TabActivity extends AppCompatActivity {
    private ProgressDialog mProgressDialog;
    MainActivity.TabType thisTab;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_tab);

        thisTab = (MainActivity.TabType) getIntent().getSerializableExtra("thisTabIs");

        TextView contentView = (TextView) findViewById(R.id.tabText);
        if(thisTab == MainActivity.TabType.TRAVEL) {
            contentView.setText(getTravelFeed());
        }else if(thisTab == MainActivity.TabType.FOOD) {
            contentView.setText(getFoodFeed());
        }else if(thisTab == MainActivity.TabType.SHOP) {
            contentView.setText(getShopFeed());
        }else if(thisTab == MainActivity.TabType.PERSONAL) {
            contentView.setText(getPersonalFeed());
        }
    }

    private String getPersonalFeed()
    {
        Element content;
        ArrayList<String[]> feedItems = new ArrayList<>();
        String feedString = "";
        final DateFormat dateFormat = new SimpleDateFormat("EEE, dd MMM yyyy HH:mm:ss zzz");

        for(int i = 0; i<MainActivity.getTab(3).getSize();i++){
            content = MainActivity.getTab(3).getInformation(i).getDocument();
            String siteTitle = content.select("title").first().text();
            Elements items = content.select("item");
            for(Element item: items){
                String [] a = {
                        siteTitle,
                        item.select("title").text(),
                        item.select("link").text(),
                        Jsoup.parse(item.select("description").text()).text(),
                        item.select("pubDate").text()};
                feedItems.add(a);
            }
        }


        Collections.sort(feedItems, new Comparator<String[]>() {
            public int compare(String[] item1, String[] item2) {
                try {
                    return dateFormat.parse(item1[4]).compareTo(dateFormat.parse(item2[4]));
                }catch(Exception e)
                {
                    System.out.println(e);
                }
                return 0;
            }
        });



//
//        for(int i=0;i<feedItems.size()-1;i++){
//            int first = i;
//            for(int j=i+1;j<feedItems.size();j++){
//                try{
//                    Date one = dateFormat.parse(feedItems.get(j)[5]);
//                    Date two = dateFormat.parse(feedItems.get(first)[5]);
//                    if(one.after(two)){
//                        first = j;
//                    }
//
//                }catch(Exception e){
//                    System.out.println(e);
//                }
//            }
//            if(first != i){
//                String [] temp = feedItems.get(i).clone();
//                feedItems.set(i,feedItems.get(first).clone());
//                feedItems.set(first,temp);
//            }
//        }

        for(String [] item: feedItems)
        {
            String site = item[0];
            String title = item[1];
            String url = item[2];
            String description = item[3];
            String date = item[4];
            feedString = feedString + site + ":\n" + title + "\n" + description + "\n" + url + "\n\n";
        }
        return feedString;
    }



    private String getShopFeed()
    {
        Element content;
        String feedString = "";
        for(int i = 0; i<MainActivity.getTab(0).getSize();i++)
        {
            content = MainActivity.getTab(0).getInformation(i).getDocument();
            Elements detail = content.select("div.section.default-body").select("p");
            for(Element p: detail)
            {
                feedString = feedString + p.text() + "\n";
            }
            feedString = feedString + "\n";
        }
        return feedString;
    }

    private String getTravelFeed()
    {
        Element content;
        String feedString = "";
        for(int i=1; i<MainActivity.getTab(1).getSize();i++)
        {
            content = MainActivity.getTab(1).getInformation(i).getDocument();
            Elements date = content.select("div.news-date-cont");
            Elements month = content.select("div.news-content.fl");
            for(int j=0;j<date.size();j++)
            {
                feedString = feedString + date.get(j).text()+"\n";
                feedString = feedString + month.get(j).text()+"\n\n";
            }
        }
        feedString = feedString + "\nBus Times\n";
        content = MainActivity.getTab(1).getInformation(0).getDocument().select("table").get(0);
        Elements rows = content.select("tr");
        for(Element row: rows)
        {
            for(Element cell: row.select("td"))
            {
                feedString = feedString + cell.text() + "\n";
            }
            feedString = feedString + "\n";
        }
        return feedString;
    }




    private String getFoodFeed()
    {
        Element content;
        String feedString = "";
        for(int i=0; i<MainActivity.getTab(2).getSize();i++)
        {
            content = MainActivity.getTab(2).getInformation(i).getDocument();
            Elements column = content.select("div#column-3");
            feedString = feedString + column.select(".portlet-title-text").text() + "\n";
            feedString = feedString + column.select(".portlet-content").text() + "\n\n";
            column = content.select("div#column-4");
            feedString = feedString + column.select(".portlet-title-text").text() + "\n";
            feedString = feedString + column.select(".portlet-content").text() + "\n\n";
            column = content.select("div#column-5");
            feedString = feedString + column.select(".portlet-title-text").text() + "\n";
            feedString = feedString + column.select(".portlet-content").text() + "\n\n";
            column = content.select("div#column-6");
            feedString = feedString + column.select(".portlet-title-text").text() + "\n";
            feedString = feedString + column.select(".portlet-content").text() + "\n\n\n";
        }
        return feedString;
    }




    //runs async task to update feed
    public void refreshContent(View view) { new RefreshContent().execute(); }
    private class RefreshContent extends AsyncTask<Void, Void, Void> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            mProgressDialog = new ProgressDialog(TabActivity.this);
            mProgressDialog.setMessage("Updating...");
            mProgressDialog.setIndeterminate(false);
            mProgressDialog.show();
        }

        @Override
        protected Void doInBackground(Void... params) {
            if(thisTab == MainActivity.TabType.SHOP)
            {
                MainActivity.getTab(0).updateTab();
            }else if(thisTab == MainActivity.TabType.TRAVEL)
            {
                MainActivity.getTab(1).updateTab();
            }else if(thisTab == MainActivity.TabType.FOOD)
            {
                MainActivity.getTab(2).updateTab();
            }else if(thisTab == MainActivity.TabType.PERSONAL)
            {
                MainActivity.getTab(3).updateTab();
            }
            return null;
        }

        @Override
        protected void onPostExecute(Void result) {
            // Set title into TextView
            TextView contentView = (TextView) findViewById(R.id.tabText);
            if(thisTab == MainActivity.TabType.TRAVEL) {
                contentView.setText(getTravelFeed());
            }else if(thisTab == MainActivity.TabType.FOOD) {
                contentView.setText(getFoodFeed());
            }else if(thisTab == MainActivity.TabType.SHOP) {
                contentView.setText(getShopFeed());
            }else if(thisTab == MainActivity.TabType.PERSONAL) {
                contentView.setText(getPersonalFeed());
            }
            mProgressDialog.dismiss();
        }
    }




    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_addRemove) {
            return true;
        }
        return super.onOptionsItemSelected(item);
    }


    public void gotoTravel(View view)
    {
        TextView contentView = (TextView) findViewById(R.id.tabText);
        contentView.setText(getTravelFeed());
        thisTab = MainActivity.TabType.TRAVEL;
//        Intent intent = new Intent(this,TabActivity.class);
//        intent.putExtra("thisTabIs", MainActivity.TabType.TRAVEL);
//        startActivity(intent);
    }

    public void gotoShop(View view)
    {
        TextView contentView = (TextView) findViewById(R.id.tabText);
        contentView.setText(getShopFeed());
        thisTab = MainActivity.TabType.SHOP;
//        Intent intent = new Intent(this,TabActivity.class);
//        intent.putExtra("thisTabIs", MainActivity.TabType.SHOP);
//        startActivity(intent);

    }

    public void gotoFood(View view)
    {
        TextView contentView = (TextView) findViewById(R.id.tabText);
        contentView.setText(getFoodFeed());
        thisTab = MainActivity.TabType.FOOD;
//        Intent intent = new Intent(this,TabActivity.class);
//        intent.putExtra("thisTabIs", MainActivity.TabType.FOOD);
//        startActivity(intent);
    }

    public void gotoPersonal(View view)
    {
        TextView contentView = (TextView) findViewById(R.id.tabText);
        contentView.setText(getPersonalFeed());
        thisTab = MainActivity.TabType.PERSONAL;
//        Intent intent = new Intent(this,TabActivity.class);
//        intent.putExtra("thisTabIs", MainActivity.TabType.PERSONAL);
//        startActivity(intent);
    }
    public void addRemoveSource(MenuItem item){
        Intent intent = new Intent(this,AddRemoveActivity.class);
        startActivity(intent);
    }
}
