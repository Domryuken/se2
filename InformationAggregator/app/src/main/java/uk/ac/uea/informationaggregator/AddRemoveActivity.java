package uk.ac.uea.informationaggregator;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;

import java.io.IOException;
import java.io.OutputStream;
import java.io.OutputStreamWriter;

import java.io.Writer;

import uk.ac.uea.framework.implementation.AndroidFileIO;


public class AddRemoveActivity extends AppCompatActivity {

//    AndroidFileIO androidFileIO = new AndroidFileIO(AddRemoveActivity.this);

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_remove);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_add_remove, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    public void addNewSource(View view) throws IOException{
        EditText newSource = (EditText)findViewById(R.id.newSource);
        String string = newSource.getText().toString();
        AndroidFileIO fileIO = new AndroidFileIO(AddRemoveActivity.this);
        OutputStream out = fileIO.writeFile("/sources.txt");
        Writer writer = new OutputStreamWriter(out, "UTF-8");
        writer.write(string);
        writer.close();

    }
}
