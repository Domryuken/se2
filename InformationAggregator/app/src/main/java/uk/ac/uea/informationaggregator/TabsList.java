package uk.ac.uea.informationaggregator;

/**
 * Created by sky13nmu on 27/11/2015.
 */
public class TabsList
{

    private Tab[] tabsList =
            {new Tab("Shop"), new Tab("Travel"), new Tab("Food"), new Tab("Personal")};


    TabsList()
    {
//        tabsList[0]

        tabsList[0].addInformation("http://uea.unioncloud.org/main-menu/eat-drink-shop/shop");
        tabsList[0].addInformation("http://uea.unioncloud.org/main-menu/eat-drink-shop/bars");
        tabsList[0].addInformation("http://uea.unioncloud.org/main-menu/eat-drink-shop/unio");

        tabsList[1].addInformation("https://portal.uea.ac.uk/estates/travel-and-transport/public-transport");
        tabsList[1].addInformation("http://www.firstgroup.com/norfolk-suffolk/news-and-service-updates/news");

        tabsList[2].addInformation("https://portal.uea.ac.uk/campus-kitchen/cafes-and-restaurants/bio-cafe");
        tabsList[2].addInformation("https://portal.uea.ac.uk/campus-kitchen/cafes-and-restaurants/blend");
        tabsList[2].addInformation("https://portal.uea.ac.uk/campus-kitchen/cafes-and-restaurants/cafe-57");
        tabsList[2].addInformation("https://portal.uea.ac.uk/campus-kitchen/cafes-and-restaurants/vista");
        tabsList[2].addInformation("https://portal.uea.ac.uk/campus-kitchen/cafes-and-restaurants/zest");
        tabsList[2].addInformation("https://portal.uea.ac.uk/campus-kitchen/cafes-and-restaurants/ziggys-espresso-bar");


//        Didnt finish the add/remove activity so these are the links that were used for testing purposes
        tabsList[3].addInformation("http://feeds.feedburner.com/Makeuseof");
        tabsList[3].addInformation("https://www.nasa.gov/rss/dyn/breaking_news.rss");


    }


    public Tab getTab(int givenPosition)
    {
        if(givenPosition < tabsList.length)
        {
            return tabsList[givenPosition];
        }
        return null;
    }

}
