package uk.ac.uea.informationaggregator;

import java.util.ArrayList;

import uk.ac.uea.informationaggregator.Information;

/**
 * Created by sky13nmu on 27/11/2015.
 */
public class Tab {

    private String name;
    private ArrayList<Information> informationList = new ArrayList<>();

    public Tab(){}

    public Tab(String givenName)
    {
        name = givenName;
    }

    public String getName()
    {
        return name;
    }

    public int getSize()
    {
        return informationList.size();
    }

    public void setName(String newName)
    {
        name = newName;
    }

    public Information getInformation(int a)
    {
        return informationList.get(a);
    }

    public void setInformation(Information newInfo, int a)
    {
        informationList.set(a, newInfo);
    }

    public void addInformation(String newURL)
    {
        informationList.add(informationList.size(),new Information(newURL));
    }

    public void removeInformation(int a)
    {
        informationList.remove(a);
    }

    public void updateTab()
    {
        for(int i = 0;i<informationList.size();i++)
        {
            informationList.get(i).updateInfo();
        }
    }

}
