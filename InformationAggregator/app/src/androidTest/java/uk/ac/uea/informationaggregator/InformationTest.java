


package uk.ac.uea.informationaggregator;

import junit.framework.TestCase;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;

/**
 * Created by Dylan on 28/01/2016.
 */
public class InformationTest extends TestCase {
    String testUrl = "http://www.bbc.co.uk/";
    Information information = new Information(testUrl);
    Document testDocument;
    Boolean done;
    public void runTest(){
        testSetURL(testUrl);
        if (testUrl.equals(testGetUrl())) {
            done = true;
        }
        else{
            done = false;
        }
        testUpdate();
        if (testDocument.equals(testGetDocument())){
            done = true;
        } else{
            done = false;
        }
        thenVerifySuccess();
    }
    public void thenVerifySuccess() {
        assertTrue(done);
    }

    public void testUpdate(){
        try{
            testDocument = Jsoup.connect(testUrl).timeout(60000).get();
            System.out.println();
        }catch(Exception e) {
            System.out.println("THIS ERROR IS" + e);
        }
    }
    public Document testGetDocument(){
        return information.getDocument();


    }
    public void testSetURL(String testURL) {
        try {
            information.setURL(testUrl);
            done = true;
        } catch (Exception e) {
            done = false;
        }
    }
    public String testGetUrl(){
        return
                information.getURL();
    }


}





