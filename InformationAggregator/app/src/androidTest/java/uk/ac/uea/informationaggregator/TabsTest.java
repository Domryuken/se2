package uk.ac.uea.informationaggregator;

import junit.framework.TestCase;

import java.util.ArrayList;


/**
 * Created by Dylan on 28/01/2016.
 */
public class TabsTest extends TestCase{
    boolean done;
    Tab tabTest;
    ArrayList<Information> informationList = new ArrayList<>();
    public void runTest(){
        Information testInfo = null;
        formTab();
        thenVerifySuccess();
        getTabTest();
        addInformation();
        if (!(testInfo.equals(getInformationTest()))){
            done = false;
        }else{
            done = true;
        }
        testUpdate();
        testRemove();
        if ((testInfo.equals(getInformationTest()))){
            done = false;
        }else{
            done = true;
        }
        String testForTheName = "testName";
        testSetName(testForTheName);
        if (testForTheName.equals(testName())){
            done = true;
        }else{
            done = false;
        }
    }
    public void thenVerifySuccess() {
        assertTrue(done);
    }

    public void formTab(){
        tabTest = new Tab("tab1");
        String url = "testURL";

    }
    public void testSetName(String name){
        tabTest.setName(name);
    }
    public String testName(){
        return tabTest.getName();
    }

    public void addInformation() {
        Information newInfo = null;
        try {
            informationList.set(0, newInfo);
            done = true;
        } catch (Exception e) {
            done = false;
        }
    }
    public Information getInformationTest() {
        return informationList.get(0);
    }
    public void getTabTest(){
        try {
            tabTest.getName();
            done = true;
        } catch (Exception e) {
            done = false;
        }
    }
    public void testUpdate(){
        try {
            tabTest.updateTab();
            done = true;
        } catch (Exception e) {
            done = false;
        }
    }
    public void testRemove(){
        try {
            tabTest.removeInformation(0);
            done = true;
        } catch (Exception e) {
            done = false;
        }
    }


}
