package uk.ac.uea.framework;

import android.app.Activity;
import uk.ac.uea.framework.implementation.AndroidAudio;

public class SoundResource {

    private Audio myAudio;
    private Sound mySound;

    SoundResource(Activity act)
    {
        myAudio = new AndroidAudio(act);
    }

    public void load(String resourcePath) {
        mySound = myAudio.createSound(resourcePath);
    }

    public void play()
    {
        mySound.play((float)0.9);
    }

    public void stop()
    {
        mySound.stop();
    }

}
