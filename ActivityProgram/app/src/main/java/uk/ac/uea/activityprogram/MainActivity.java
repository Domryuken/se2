package uk.ac.uea.activityprogram;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.os.Parcelable;
import android.preference.PreferenceManager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.view.menu.ActionMenuItemView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.CalendarView;
import android.widget.ListView;

import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.URL;
import java.net.URLConnection;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.concurrent.TimeUnit;

import uk.ac.uea.framework.implementation.AndroidFileIO;


public class MainActivity extends AppCompatActivity {

    private CalendarView calendarView;
    private ListView listView;
    private int id;
    private String url = "https://calendar.google.com/calendar/ical/" +
            "vse63tlci6bc49b68qq0hojrjg%40group.calendar.google.com/public/basic.ics";
    private String filename = "IcsFile";
    private EventArrayAdapter eventArrayAdapter;
    InfoRetriever infoRetriever = new InfoRetriever();

    public final static String ITEM_ID = "<uk.ac.uea.activityprogram>.<activityprogram>.ITEM_ID";

    /**
     * Create the main activity.
     *
     * @param savedInstanceState previously saved instance data.
     */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar myToolbar = (Toolbar) findViewById(R.id.app_bar);
        setSupportActionBar(myToolbar);
        if (isDeviceOnline()) {
            new DownloadFile().execute(url);
            autoUpdate();
        } else {
            alertNoConnection();
        }
//Stuff in onPostExecute moved from here moved from here
    }

    /**
     * displays alert dialog if device has no connection.
     */
    public void alertNoConnection() {
        new AlertDialog.Builder(this)
                .setTitle("Alert!")
                .setMessage("Establish network connection")
                .setNeutralButton("Finish", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        finish();
                    }
                }).setIcon(android.R.drawable.ic_dialog_alert).show();
    }

    /**
     * populate Calendar view and List view with eventList.
     *
     * @param eventList list of Events
     */
    public void setView(final List<Event> eventList) {
        calendarView = (CalendarView) findViewById(R.id.calendarView1);
        listView = (ListView) findViewById(R.id.AssignListView);
        eventArrayAdapter = new EventArrayAdapter(getApplicationContext(), R.layout.single_event_item);
        Parcelable state = listView.onSaveInstanceState();
        listView.setAdapter(eventArrayAdapter);
        listView.onRestoreInstanceState(state);
        //DATE CHECKER MOVED FROM HERE
        //calls dateChecker passes calendar view and list of events
        dateChecker(calendarView, eventList);
    }

    /**
     * forces update when user clicks update item.
     *
     * @param button view of ActionMenuItem to check for user item click
     */
    public void manualUpdate(ActionMenuItemView button) {
        if (isDeviceOnline()) {
            button.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    new DownloadFile().execute(url);
                    final List<Event> eventList = parseFile();
                    dateChecker(calendarView, eventList);
                    eventArrayAdapter.notifyDataSetChanged();
                    listView.refreshDrawableState();
                }
            });
        } else {
            alertNoConnection();
        }
    }

    /**
     * checks if device is online.
     *
     * @return true if device and network connection else false
     */
    private boolean isDeviceOnline() {
        ConnectivityManager connMgr =
                (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo networkInfo = connMgr.getActiveNetworkInfo();
        return (networkInfo != null && networkInfo.isConnected());
    }

    /**
     * Used to automatically update calendar on set intervals which the user can
     * change in settings. default update interval : 5 mins.
     */
    public void autoUpdate() {
        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(getBaseContext());
        final String update = preferences.getString("list", "5");
        final int updateInterval = Integer.parseInt(update);
        final long alarmTime = TimeUnit.MINUTES.toMillis(updateInterval);

        Log.e("Milli", "seconds" + alarmTime);
        final Handler mHandler = new Handler();
        /**
         * starts new Thread to call eDownloadFile asyncTask with a set delay
         */
        Runnable runnable = new Runnable() {
            @Override
            public void run() {
                try {
                    new DownloadFile().execute(url);
                    mHandler.postDelayed(this, alarmTime);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        };
        // repetitively call runnable thread.
        mHandler.postDelayed(runnable, alarmTime);
    }

    /**
     * calls to parse file through InfoRetriever and save file in storage.
     *
     * @return list of events from calendar
     */
    public final List<Event> parseFile() {
        AndroidFileIO androidFileIO = new AndroidFileIO(MainActivity.this);
        FileInputStream fin = null;
        try {
            fin = new FileInputStream(androidFileIO.getFilePath(filename));
        } catch (Exception e) {
            e.printStackTrace();
        }
        return infoRetriever.parseEvent(fin);
    }

    /**
     * calls to parse ids through InfoRetriever class.
     *
     * @return list of ids of calendar.
     */
    public final List<String> parseIDs() {
        FileInputStream fin = null;
        AndroidFileIO androidFileIO = new AndroidFileIO(MainActivity.this);
        try {
            fin = new FileInputStream(androidFileIO.getFilePath(filename));
        } catch (Exception e) {
            e.printStackTrace();
        }
        final List<String> eventList = infoRetriever.parseIDs(fin);
        return eventList;
    }

    /**
     * listens to selected date change and changes list view of events accordingly
     *
     * @param calenderView calendar view which the user is looking at
     * @param eventList    list of events
     */
    public void dateChecker(CalendarView calenderView, final List<Event> eventList) {
        id = 0;
        calendarView.setOnDateChangeListener(new CalendarView.OnDateChangeListener() {

            public void onSelectedDayChange(CalendarView view, int year, int month, int day) {
                listView.invalidate();
                listView.setAdapter(eventArrayAdapter);
                String date = year + "-" + (month + 1) + "-" + day;
                Date eventDate = infoRetriever.formatDate(date);
                eventArrayAdapter.removeAll();
                final List<Event> events = new ArrayList<Event>();

                for (Event event1 : eventList) {
                    if (event1.getDate().compareTo(eventDate) == 0) {
                        event1.setUid(id++);
                        eventArrayAdapter.add(event1);
                        events.add(event1);
                    }
                }
                /**listen to item click which is used to MainActivity
                 *  which contains further detail of selected events
                 **/
                listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {

                    @Override
                    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                        Intent i = new Intent(MainActivity.this, EventActivity.class);
                        i.putExtra(ITEM_ID, events.get(position));
                        i.putExtra("UID", events.get(position).getUid());
                        startActivity(i);
                    }
                });
            }
        });
    }

    /**
     * AsycTask used to download ics file in the background and
     * show result in main UI Thread
     */
    public class DownloadFile extends AsyncTask<String, String, Boolean> {
        /**
         * invoked on the background thread which download the ics file.
         *
         * @param zipUrl url to download ics file
         * @return true if download was successful else false
         */
        @Override
        protected Boolean doInBackground(String... zipUrl) {
            int count = 0;
            AndroidFileIO androidFileIO = new AndroidFileIO(MainActivity.this);
            boolean success = false;
            try {
                URL url = new URL(zipUrl[0]);
                URLConnection urlConnection = url.openConnection();
                urlConnection.connect();

                int fileLength = urlConnection.getContentLength();
                Log.d("ANDRO_ASYNC", "LENGTH OF FILE - " + fileLength);

                OutputStream outputStream = androidFileIO.writeFile(filename);
                InputStream inputStream = androidFileIO.stream(url);
                byte data[] = new byte[1024];
                long total = 0;
                while ((count = inputStream.read(data)) != -1) {
                    outputStream.write(data, 0, count);
                    success = true;
                }
                outputStream.flush();
                outputStream.close();
                inputStream.close();

            } catch (Exception e) {
                e.printStackTrace();

            }
            return success;
        }

        /**
         * before executing asynctask
         */
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        /**
         * after execution set views with event list and check for
         * manual update prompt
         *
         * @param bool boolean true if execution was successful
         */
        @Override
        protected void onPostExecute(Boolean bool) {
            if (bool == true) {
                //parseFile MOVED FROM HERE
                final List<Event> eventList = parseFile();
                setView(eventList);

                ActionMenuItemView button = (ActionMenuItemView) findViewById(R.id.download);
                //MANUAL UPDATE MOVED FROM HERE
                manualUpdate(button);
            }
        }
    }

    /**
     * Inflate the menu; this adds items to the action bar if it is present.
     *
     * @param menu
     * @return returns true
     */
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        //
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    /**
     * when an option is selected by the user execute method
     * settings, exit
     *
     * @param menuItem item chosen by the user
     * @return returns true.
     */
    @Override
    public boolean onOptionsItemSelected(MenuItem menuItem) {
        switch (menuItem.getItemId()) {
            case R.id.action_settings:
                Intent intent = new Intent(this, SettingsActivity.class);
                startActivity(intent);
                break;
            case R.id.exit:
                finish();
                break;
        }
        return false;
    }
}