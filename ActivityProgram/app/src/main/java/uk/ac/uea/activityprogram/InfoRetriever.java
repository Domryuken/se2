package uk.ac.uea.activityprogram;


/**
 * Created by sanjeev on 10/01/2016.
 */

import net.fortuna.ical4j.data.CalendarBuilder;
import net.fortuna.ical4j.model.Calendar;
import net.fortuna.ical4j.model.Component;
import net.fortuna.ical4j.model.Property;
import net.fortuna.ical4j.model.PropertyList;

import java.io.InputStream;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

public class InfoRetriever {
    /**
     * default constructor
     */
    public InfoRetriever() {
    }

    /**
     * specifically parses calendar ids through inputstream
     *
     * @param inputStream for the ics calendar file
     * @return list of strings containing ids
     */
    public List<String> parseIDs(InputStream inputStream) {
        List<String> calendarIds = new ArrayList<>();
        CalendarBuilder calendarBuilder = new CalendarBuilder();
        Calendar calendar = null;
        try {
            calendar = calendarBuilder.build(inputStream);
        } catch (Exception e) {
            e.printStackTrace();
        }
        if (calendar != null) {
            System.out.println("value: " + calendar);
            PropertyList propertyList = calendar.getProperties();
            propertyList.toArray();
            String name = (String) propertyList.get(4);
            name.substring(name.lastIndexOf(":"));
            calendarIds.add(name);
        }
        return calendarIds;
    }
    /**
     * parses entire calendar; gets all properties of calendar
     * and save as Event type to be added to the list of events
     *
     * @param inputStream for the ics calendar file
     * @return list of events
     */
    public List<Event> parseEvent(InputStream inputStream) {
        List<Event> allEvents = new ArrayList<>();

        CalendarBuilder calendarBuilder = new CalendarBuilder();
        Calendar calendar = null;
        try {
            calendar = calendarBuilder.build(inputStream);
        } catch (Exception e) {
            e.printStackTrace();
        }
        for (Iterator i = calendar.getComponents().iterator(); i.hasNext(); ) {
            Component component = (Component) i.next();
            Event event = new Event();
            System.out.println("Component [" + component.getName() + "]");
            String name;
            for (Iterator j = component.getProperties().iterator(); j.hasNext(); ) {
                Property property = (Property) j.next();

                name = property.getName();
                getPropertyValue(name, property, event);
            }
            allEvents.add(event);
        }
        return allEvents;
    }

    public void getPropertyValue(String name, Property property, Event event) {
        switch (name) {
            case "DTSTART":
                event.setDate(property.getValue());
                event.setStime(property.getValue());
                break;
            case "DTEND":
                event.setEtime(property.getValue());
                break;
            case "UID":
                event.setId(property.getValue());
                break;
            case "DESCRIPTION":
                event.setDescription(property.getValue());
                break;
            case "LAST-MODIFIED":
                event.setLastModified(property.getValue());
                break;
            case "LOCATION":
                event.setLocation(property.getValue());
                break;
            case "SUMMARY":
                event.setTitle(property.getValue());
                break;
            default:
                break;
        }
    }

    /**
     * format dateTime in string format to date type format
     *
     * @param dateTime dateTime in original format converted to type Date
     * @return returns type Date type
     */
    public Date formatToDate(String dateTime) {
        SimpleDateFormat datetimeFormat = new SimpleDateFormat("yyyyMMdd'T'HHmmss'Z'");
        Date d = null;
        try {
            d = datetimeFormat.parse(dateTime);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return d;
    }


    /**
     * parses date in string to date type
     *
     * @param date in string format (yyyy-MM-dd)
     * @return date type
     */
    public Date formatDate(String date) {
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
        Date d = null;
        try {
            d = dateFormat.parse(date);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return d;
    }
}
