package uk.ac.uea.activityprogram;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;


import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Created by sanjeev on 15/01/2016.
 */
public class EventActivity extends AppCompatActivity {
    private Context context;
    SharedPreferences preferences;
    AlarmService alarmService;
    private int uid;
    Event event;

    /**
     * Creates event page; passes eventItems as parcel
     * and sets value to textView
     *
     * @param savedInstanceState previously saved instance data.
     */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.event_detail);
        Toolbar myToolbar = (Toolbar) findViewById(R.id.app_bar);
        setSupportActionBar(myToolbar);
        Intent intent = getIntent();
        event = (Event) intent.getParcelableExtra(MainActivity.ITEM_ID);
        uid = intent.getIntExtra("UID", 100);
        TextView eventTitle = (TextView) findViewById(R.id.eventTitle);
        TextView eventDate = (TextView) findViewById(R.id.eventDate);
        TextView eventStart = (TextView) findViewById(R.id.eventStartTime);
        TextView eventEnd = (TextView) findViewById(R.id.eventEndTime);
        TextView eventLocation = (TextView) findViewById(R.id.eventLocation);
        TextView eventDescription = (TextView) findViewById(R.id.eventDescription);
        Button setReminder = (Button) findViewById(R.id.reminder);
        Button reminderCancel = (Button) findViewById(R.id.reminderCancel);
        reminderCancel.setVisibility(View.INVISIBLE);
        eventTitle.setText(event.getTitle());
        eventDate.setText(event.getDate().toString());
        eventStart.setText(event.getStime());
        eventEnd.setText(event.getEtime());
        eventLocation.setText(event.getLocation());
        eventDescription.setText(event.getDescription());
        //check date is current of future
        checkDate(setReminder);


//        if (alarmSet()) {
//            cancelAlarm(reminderCancel, setReminder);
//            setReminder.setVisibility(View.GONE);
//            reminderCancel.setVisibility(View.VISIBLE);
//        }
        onSetReminder(event, setReminder, reminderCancel);

    }

    public boolean alarmSet() {
        return alarmService.checkAlarm(uid);
    }


    public void cancelAlarm(final Button reminderCancel, final Button setReminder) {
        if (alarmSet()) {
            setReminder.setVisibility(View.GONE);
            reminderCancel.setVisibility(View.VISIBLE);
            reminderCancel.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    boolean cancelConfirmation = alarmService.isCancelled(uid);
                    if (cancelConfirmation) {
                        reminderCancel.setVisibility(View.GONE);
                        setReminder.setVisibility(View.VISIBLE);
                    }
                }
            });
        }
    }

    /**
     * checks if date is current or in the future
     * if date is passed, hide button.
     *
     * @param setReminder reminder button
     */
    public void checkDate(Button setReminder) {
        Date date = new Date();
        if (event.getDate().compareTo(date) < 0) {
            setReminder.setVisibility(View.GONE);
        }
    }

    /**
     * find saved preference for how many minutes in
     * advance user wants to be reminded before the event.
     * Default: 5 mins
     *
     * @return minute integer
     */
    public int advanceReminderSetting() {
        preferences = PreferenceManager.getDefaultSharedPreferences(getBaseContext());
        String advance = preferences.getString("reminderList", "5");
        return Integer.parseInt(advance);
    }

    /**
     * find saved preference if user wants to be reminded
     * through sound notification
     *
     * @return true if user wants sound else false
     */
    public boolean soundSetting() {
        preferences = PreferenceManager.getDefaultSharedPreferences(getBaseContext());
        return preferences.getBoolean("checkSound", true);

    }

    /**
     * find saved preference if user wants to be reminded
     * through vibrate notification
     *
     * @return true of  user wants vibrate on else off
     */
    public boolean vibrateSettings() {
        preferences = PreferenceManager.getDefaultSharedPreferences(getBaseContext());
        return preferences.getBoolean("checkVibrate", true);
    }

    /**
     * when set reminder is clicked by user start alarm service and set notification.
     *
     * @param e           current event
     * @param setReminder button for reminder
     */
    public void onSetReminder(final Event e, final Button setReminder, final Button reminderCancel) {
        setReminder.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alarmService = startAlarmService(event);
                Date date = e.getDate();
                String fDate = new SimpleDateFormat("yyyy-MM-dd").format(date);
                String[] strings = fDate.split("-");
                int year = Integer.parseInt(strings[0]);
                int month = Integer.parseInt(strings[1]) - 1;
                int day = Integer.parseInt(strings[2]);

                String startTime = e.getStime();
                String hour = startTime.substring(0, startTime.indexOf(":"));
                String minute = startTime.substring(startTime.indexOf(":") + 1, startTime.indexOf(" "));
                int start = Integer.parseInt(hour);
                int minutes = Integer.parseInt(minute) - advanceReminderSetting();
                int sec = 0;
                int uid = e.getUid();


                //Alarm service constructor from here
                alarmService.startAlarm(start, minutes, sec, strings, uid);

                Toast.makeText(EventActivity.this, "Notification set:" + day + "/" + (month + 1) + "/" + year, Toast.LENGTH_SHORT).show();
                // cancelAlarm(reminderCancel, setReminder);
            }

        });

    }

    public AlarmService startAlarmService(Event e) {
        String[] messages = new String[4];
        messages[0] = e.getTitle();
        messages[1] = e.getStime();
        messages[2] = e.getEtime();

        boolean checkSound = soundSetting();
        boolean vibrateSound = vibrateSettings();

        alarmService = new AlarmService(EventActivity.this, messages, checkSound, vibrateSound);
        return alarmService;
    }

    /**
     * Inflate the menu; this adds items to the action bar if it is present.
     *
     * @param menu
     * @return returns true
     */
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    /**
     * when an option is selected by the user execute method
     * settings, exit
     *
     * @param menuItem item chosen by the user
     * @return returns true.
     */
    @Override
    public boolean onOptionsItemSelected(MenuItem menuItem) {
        switch (menuItem.getItemId()) {
            case R.id.action_settings:
                Intent intent = new Intent(this, SettingsActivity.class);
                startActivity(intent);
                break;
            case R.id.exit:
                finish();
                break;
        }
        return false;
    }
}
