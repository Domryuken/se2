package uk.ac.uea.activityprogram;

import android.app.AlertDialog;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.media.MediaPlayer;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Build;
import android.os.Vibrator;
import android.support.v4.app.NotificationCompat;
import android.widget.Toast;

import java.net.ContentHandler;

/**
 * Created by sanjeev on 18/01/2016.
 */
public class AlarmReceiver extends BroadcastReceiver {
    NotificationManager notificationManager;
    MediaPlayer mediaPlayer;

    /**
     * called when the Alarm receiver receives intent broadcast
     * from alarm service
     * @param context context in which receiver is running
     * @param intent intent received from alarm manager
     */
    @Override
    public void onReceive(Context context, Intent intent) {

        String text = intent.getStringExtra("message");
        String title = intent.getStringExtra("title");
        long remindTime = intent.getLongExtra("getTime", 0L);
        boolean vibAuth = intent.getBooleanExtra("checkVibrate",true);
        boolean soundAuth = intent.getBooleanExtra("checkSound", true);
        if(vibAuth)
        {
            vibrateNotification(context);
        }if(soundAuth)
        {
            soundNotification(context);
        }
        setNotification(context, title, text);


    }

    /**
     * starts vibrate notification so that when alarm starts device will vibrates
     * @param context context in which receiver is running
     */
    public void vibrateNotification(Context context) {
        Vibrator vibrateNotification = (Vibrator) context.getSystemService(context.VIBRATOR_SERVICE);
        vibrateNotification.vibrate(1000);
        Toast.makeText(context, "Event has Started", Toast.LENGTH_LONG).show();
    }
    /**
     * starts sound notification so that when alarm starts device's media player will play
     * notification ringtone
     * @param context context in which receiver is running
     */
    public void soundNotification(Context context) {
        Uri notification = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
        mediaPlayer = MediaPlayer.create(context, notification);
        mediaPlayer.start();
    }

    /**
     * set notification so that when alarm starts user is notified through
     * notification manager, when user clicks
     * notification they are navigated back to app main page
     * @param context context of receiver
     * @param title title to be set for notification
     * @param text text to be set for notification
     */

    public void setNotification(Context context, String title, String text) {
        notificationManager = (NotificationManager) context.getSystemService(context.NOTIFICATION_SERVICE);
        Notification notification = null;
        PendingIntent contIntent = PendingIntent.getActivity(context, 0, new Intent(context, MainActivity.class), 0);

        if (Build.VERSION.SDK_INT < 16) {
            notification = new Notification.Builder(context)
                    .setContentText(text).setContentTitle(title).setSmallIcon(R.drawable.common_full_open_on_phone)
                    .setAutoCancel(true).setWhen(System.currentTimeMillis())
                    .setContentIntent(contIntent).getNotification();
        } else {
            notification = new Notification.Builder(context)
                    .setContentText(text).setContentTitle(title).
                            setSmallIcon(R.drawable.common_full_open_on_phone).
                            setAutoCancel(true).setWhen(System.currentTimeMillis())
                    .setContentIntent(contIntent).build();
        }

        notificationManager.notify(R.string.app_name, notification);
    }
}

