package uk.ac.uea.activityprogram;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.util.Log;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Random;
import java.util.TimeZone;
import java.util.concurrent.TimeUnit;

/**
 * Created by sanjeev on 18/01/2016.
 */
public class AlarmService {
    private Context context;
    private PendingIntent mAlarmSender;
    Calendar calendar;
    String message = null;
    String title = null;
    int reminderID = 0;
    String date = null;
    AlarmManager am;
    Intent pending;
    public AlarmService() {
    }
    public AlarmService(Context context, String[] notification, boolean checkSound, boolean checkVibrate) {
        this.context = context;
        this.title = notification[0];
        this.message = notification[1] + " - " + notification[2];
        this.pending = new Intent(context, AlarmReceiver.class);
        pending.putExtra("message", message);
        pending.putExtra("title", title);
        pending.putExtra("checkSound", checkSound);
        pending.putExtra("checkVibrate", checkVibrate);

    }

    /**
     * generates random id for each alarm service
     *
     * @return ids of alarmService
     */
    public int generateRandom() {
        Random random = new Random();
        return random.nextInt(9999 - 1000) + 1000;
    }

    public boolean checkAlarm(int uid) {
        boolean alarmUp = (PendingIntent.getBroadcast(context, uid,
                new Intent(context, AlarmReceiver.class),
                PendingIntent.FLAG_NO_CREATE) != null);
        return alarmUp;
    }

    public boolean isCancelled( int uid) {
        boolean cancel;
        try {
            am.cancel(PendingIntent.getService(context, uid, pending, PendingIntent.FLAG_UPDATE_CURRENT));
            PendingIntent.getBroadcast(context, reminderID, pending,
                    PendingIntent.FLAG_UPDATE_CURRENT).cancel();
            cancel = true;
        } catch (Exception e) {
            cancel = false;
        }
        return cancel;
    }

    /**
     * get date and time of scheduling alarm
     * and starts Alarm manager and schedule an alarm
     *
     * @param hour    hour
     * @param min     minute
     * @param sec     second
     * @param strings string array containing year, month and day of month
     */
    public void startAlarm(int hour, int min, int sec, String[] strings, int reminderID) {
        this.reminderID = reminderID;
        mAlarmSender = PendingIntent.getBroadcast(context, reminderID, pending, PendingIntent.FLAG_UPDATE_CURRENT);
        //Set the alarm to 10 seconds from now
        calendar = Calendar.getInstance(TimeZone.getTimeZone("UTC"));

        int year = Integer.parseInt(strings[0]);
        int month = Integer.parseInt(strings[1]) - 1;
        int day = Integer.parseInt(strings[2]);
        //calendar.set(year,month, day);
        calendar.set(Calendar.YEAR, year);
        calendar.set(Calendar.MONTH, month);
        calendar.set(Calendar.DAY_OF_MONTH, day);

        calendar.set(Calendar.HOUR_OF_DAY, hour);
        calendar.set(Calendar.MINUTE, min);
        calendar.set(Calendar.SECOND, sec);

        long firstTime = calendar.getTimeInMillis();
        // Schedule the alarm!
        am = (AlarmManager) context.getSystemService(Context.ALARM_SERVICE);
        am.set(AlarmManager.RTC_WAKEUP, firstTime, mAlarmSender);
    }
}