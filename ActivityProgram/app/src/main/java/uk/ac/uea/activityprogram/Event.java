package uk.ac.uea.activityprogram;

import android.os.Parcel;
import android.os.Parcelable;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.TimeZone;

/**
 * Created by sanjeev on 28/11/2015.
 */
public class Event implements Parcelable {
    private InfoRetriever infoRetriever = new InfoRetriever();
    private String title;
    private String id;
    private int uid;
    private String location;
    private Date date;
    private String sTime;
    private String eTime;
    private String description;
    private String lastModified;
    private String stringDate;


    /**
     * default constructor
     */
    public Event() {
    }
    /**
     * constructor of type Event
     *
     * @param title        event title
     * @param id           event id
     * @param location     where the event is taking place
     * @param date         date of event
     * @param stime        start time of event
     * @param etime        end time of event
     * @param description  more detail of event
     * @param lastModified last date when event was modified
     * @param stringDate   date of event as string
     */
    public Event(String title, String id, String location, Date date, String stime, String etime,
                 String description, String lastModified, String stringDate) {
        this.title = title;
        this.id = id;
        this.location = location;
        this.date = date;
        this.sTime = stime;
        this.eTime = etime;
        this.description = description;
        this.lastModified = lastModified;
        this.stringDate = stringDate;
    }

    public int getUid() {
        return uid;
    }

    public void setUid(int uid) {
        this.uid = uid;
    }

    /**
     * get start time as string
     *
     * @return start time
     */
    public String getStime() {
        return sTime;
    }

    /**
     * format and set start time
     *
     * @param stime start time in calendar format
     */
    public void setStime(String stime) {
        Date d = infoRetriever.formatToDate(stime);
        String Starttime = formatTime(stime, d);
        this.sTime = Starttime;
    }

    /**
     * format time to match format 12:22 am
     *
     * @param stime start time in origal format
     * @param d     Date format
     * @return startTime in string
     */
    public String formatTime(String stime, Date d) {
        String time = stime.substring(stime.indexOf("T"));
        String startTime = null;
        SimpleDateFormat timeFormat = new SimpleDateFormat("HH:mm a");
        startTime = timeFormat.format(d);
        return startTime;
    }

    /**
     * @return description
     */
    public String getDescription() {
        return description;

    }
    /**
     * @return stringDate
     */
    public String getStringDate() {
        return stringDate;
    }
    /**
     * @return end time
     */
    public String getEtime() {
        return eTime;
    }
    /**
     * @return lastModified
     */
    public String getLastModified() {
        return lastModified;
    }

    /**
     * @param lastModified last modified date
     */
    public void setLastModified(String lastModified) {
        this.lastModified = lastModified;
    }

    /**
     * @param description more detail of event
     */
    public void setDescription(String description) {
        this.description = description;
    }

    /**
     * format and set end time
     *
     * @param etime end time in calendar format
     */
    public void setEtime(String etime) {
        Date d = infoRetriever.formatToDate(etime);
        String Endtime = formatTime(etime, d);
        eTime = Endtime;
    }

    /**
     * @return type Date
     */
    public Date getDate() {
        return date;
    }

    /**
     * parse dateTime string in yyyyMMdd format set as date
     *
     * @param dateTime string dateTime
     */
    public void setDate(String dateTime) {
        SimpleDateFormat datetimeFormat = new SimpleDateFormat("yyyyMMdd");
        String date = dateTime.substring(0, dateTime.indexOf("T"));
        Date d = null;
        try {
            d = datetimeFormat.parse(dateTime);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        this.date = d;
    }

    /**
     * @return event id
     */
    public String getId() {

        return id;
    }

    /**
     * @param id event id
     */
    public void setId(String id) {
        this.id = id;
    }

    /**
     * @return location of event
     */
    public String getLocation() {

        return location;
    }

    /**
     * @param location location
     */
    public void setLocation(String location) {
        this.location = location;
    }

    /**
     * @return event title
     */
    public String getTitle() {

        return title;
    }

    /**
     * @param title event title
     */
    public void setTitle(String title) {
        this.title = title;
    }

    /**
     * parcelable implements method of parcelable interface
     *
     * @return 0
     */
    @Override
    public int describeContents() {
        return 0;
    }

    /**
     * write primitive data types to parcel
     *
     * @param dest  parcel in which object should be written
     * @param flags how the object should be written
     */
    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(title);
        dest.writeString(id);
        dest.writeString(location);
        dest.writeString(sTime);
        dest.writeString(eTime);
        dest.writeString(description);
        dest.writeString(lastModified);
        Calendar cal = Calendar.getInstance();
        cal.setTime(date);
        String DateString = cal.get(Calendar.YEAR) + "-" + cal.get(Calendar.MONTH)
                + "-" + cal.get(Calendar.DAY_OF_MONTH);
        dest.writeString(DateString);

    }

    public static final Parcelable.Creator<Event> CREATOR = new Parcelable.Creator<Event>() {
        /**
         * create Event from parcel
         * @param in parcel object
         * @return new Event
         */
        public Event createFromParcel(Parcel in) {
            return new Event(in);
        }

        /**
         * create new event array
         * @param size size of array
         * @return event Array
         */
        public Event[] newArray(int size) {
            return new Event[size];
        }
    };

    /**
     * example constructor that takes a Parcel
     * and gives you an object populated with it's values
     *
     * @param in Parcel object
     */
    //
    private Event(Parcel in) {
        title = in.readString();
        id = in.readString();
        location = in.readString();
        sTime = in.readString();
        eTime = in.readString();
        description = in.readString();
        lastModified = in.readString();
        String str = in.readString();
        SimpleDateFormat dateFormat;
        if (str.length() < 10) {
            String[] strings = str.split("-");
            int year = Integer.parseInt(strings[0]);
            int month = Integer.parseInt(strings[1]);
            int day = Integer.parseInt(strings[2]);

            int hour = Integer.parseInt(sTime.substring(0, sTime.indexOf(":")));
            int min = Integer.parseInt(sTime.substring(sTime.indexOf(":") + 1, sTime.indexOf(" ")));
//            try {
            Calendar cal = Calendar.getInstance(TimeZone.getTimeZone("UTC"));

            cal.set(Calendar.YEAR, year);
            cal.set(Calendar.MONTH, month);
            cal.set(Calendar.DAY_OF_MONTH, day);
            cal.set(Calendar.HOUR_OF_DAY, hour);
            cal.set(Calendar.MINUTE, min);
            cal.set(Calendar.SECOND, 0);
            this.date = cal.getTime();


        }
    }
}
