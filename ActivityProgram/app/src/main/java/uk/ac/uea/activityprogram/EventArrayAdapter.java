package uk.ac.uea.activityprogram;


import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by sanjeev on 14/01/2016.
 */
public class EventArrayAdapter extends ArrayAdapter<Event> {
    private List<Event> events = new ArrayList<>();
    static class EventViewHolder
    {
        TextView name;
        TextView time;

    }
    public EventArrayAdapter(Context context, int resource) {
        super(context, resource);
    }

    /**
     * add event to event list
     * @param event Event object
     */
    public void add(Event event) {
        events.add(event);
        super.add(event);
    }

    /**
     *
     * @return size of event list
     */
    @Override
    public int getCount() {
        return events.size();
    }

    /**
     * get specific item
     * @param item position of event
     * @return event
     */
    @Override
    public Event getItem(int item) {
        return events.get(item);
    }

    /**
     * remove all events from event list
     */
    public void removeAll()
    {
        events.clear();
    }

    /**
     * Get a View that displays the data at the specified position in the data set.
     * @param position position of item
     * @param convertView old view to reuse
     * @param parent parent of new view
     * @return new view to be returned
     */
    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View row = convertView;
        EventViewHolder viewHolder;
        if(row==null){
            LayoutInflater inflater = (LayoutInflater) this.getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            row = inflater.inflate(R.layout.single_event_item, parent,false);
            viewHolder = new EventViewHolder();
            viewHolder.name = (TextView) row.findViewById(R.id.eventName);
            viewHolder.time = (TextView) row.findViewById(R.id.eventTime);
            row.setTag(viewHolder);
        }else{
            viewHolder = (EventViewHolder) row.getTag();
        }
        Event e = getItem(position);
        viewHolder.name.setText(e.getTitle());
        viewHolder.time.setText(e.getStime());
        return row;
    }
}

