package uk.ac.uea.activityprogram;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.ListPreference;
import android.preference.PreferenceFragment;

import java.util.List;

/**
 * Created by sanjeev on 19/01/2016.
 */

public class SettingsActivity extends Activity {
    /**
     * onCreate commits Preference Fragment to activity
     * @param savedInstanceState  previously saved instance data.
     */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getFragmentManager().beginTransaction().replace(android.R.id.content, new SettingsFragment()).commit();
    }

    /**
     *  extends PreferenceFragment and is used to inflate the Settings xml and add
     *  Preference to current list
     */
    public static class SettingsFragment extends PreferenceFragment {
        ListPreference listPref;

        @Override
        /**
         * inflates xml resources and add preference heirarchy to current preference
         * @param savedInstanceState previously saved instance data.
         */
        public void onCreate(Bundle savedInstanceState) {
            super.onCreate(savedInstanceState);
            //inflates setting xml and add preference hierarchy
            addPreferencesFromResource(R.xml.setting);
            listPref = (ListPreference) findPreference("list");
        }
    }
}
