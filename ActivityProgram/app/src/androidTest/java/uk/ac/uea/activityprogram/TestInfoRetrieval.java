package uk.ac.uea.activityprogram;

import android.test.AndroidTestCase;

import java.util.List;

/**
 * Created by sanjeev on 21/01/2016.
 */
public class TestInfoRetrieval extends AndroidTestCase {
    boolean parseSuccess = false;

    public void testInfoRetrieve()
    {
        testParseFile();
        testParseIds();
        assertTrue(parseSuccess);
    }
    public void testParseFile() {
        MainActivity mainActivity = new MainActivity();
        try {
            List<Event> eventList = mainActivity.parseFile();
            parseSuccess = true;
        } catch (Exception e) {
            parseSuccess = false;
        }
    }
    public void testParseIds() {
        MainActivity mainActivity = new MainActivity();
        try {
            List<String> stringList = mainActivity.parseIDs();
            parseSuccess = true;
        } catch (Exception e) {
            parseSuccess = false;
        }
    }

}
