package uk.ac.uea.activityprogram;

import android.test.AndroidTestCase;
import android.view.View;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Created by sanjeev on 21/01/2016.
 */
public class EventArrayAdapterTest extends AndroidTestCase {
    private EventArrayAdapter eventArrayAdapter;

    private Event event1;
    private Event event2;

    public EventArrayAdapterTest() {
        super();
    }

    protected void setUp() throws Exception {
        super.setUp();
        Date d = new Date();
        List<Event> eventList = new ArrayList<>();
        event1 = new Event("title", "id", "location", d, "stime", "etime",
                "description", "lastModified", "stringDate");
        event2 = new Event("title2", "id2", "location2", d, "stime2", "etime2",
                "description2", "lastModified2", "stringDate2");
        eventList.add(event1);
        eventList.add(event2);
        for (Event event : eventList) {
            eventArrayAdapter.add(event);
        }
    }

    public void testGetItem() {
        assertEquals("Sdfsdf", event1.getTitle(), ((Event) eventArrayAdapter.getItem(0)).getTitle());
    }

    public void testGetItemId() {
        assertEquals("Wrong ID.", 0, eventArrayAdapter.getItemId(0));
    }

    public void testViews() {
        View view = eventArrayAdapter.getView(0, null, null);

        TextView name = (TextView) view
                .findViewById(R.id.eventName);

        TextView time = (TextView) view
                .findViewById(R.id.eventTime);

        //On this part you will have to test it with your own views/data
        assertNotNull("View is null. ", view);
        assertNotNull("Name TextView is null. ", name);
        assertNotNull("Time TextView is null. ", time);

        assertEquals("Time doesn't match.", event1.getTitle(), name.getText());
        assertEquals("Time doesn't match.", event1.getStime(), time.getText());
    }
}

