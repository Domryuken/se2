package uk.ac.uea.activityprogram;

import android.os.Bundle;
import android.os.Parcel;
import android.os.Parcelable;
import android.test.InstrumentationTestCase;

import junit.framework.Test;
import junit.framework.TestCase;

import java.util.Date;

import static org.apache.commons.lang.builder.EqualsBuilder.reflectionEquals;

/**
 * Created by sanjeev on 21/01/2016.
 */
public class testEvent extends TestCase {
    Event event;
    boolean dateFormated;

    public void testEventCheck() {
        formatFromDateString();
        formatFromDateTimeString();
        thenVerifySuccess();
    }
    public void thenVerifySuccess() {
        assertTrue(dateFormated);
    }

    public void formatFromDateString() {
        event = new Event();
        String date = "1999-01-01";
        Date d;
        try {
            d = event.formatDate(date);
            dateFormated = true;
        } catch (Exception e) {
            dateFormated = false;
        }
    }

    public void formatFromDateTimeString() {
        String currentformat = "yyyyMMdd'T'HHmmss'Z'";
        event = new Event();
        String date = "19990101T010233Z";
        try {
            event.formatToDate(date);
            dateFormated = true;
        } catch (Exception e) {
            dateFormated = false;
        }
    }
}
